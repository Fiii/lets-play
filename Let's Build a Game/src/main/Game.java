package main;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;

		public class Game extends Canvas implements Runnable {
		private static final long serialVersionUID = 1L;
		public static final int WIDTH = 640, HEIGHT = WIDTH / 12*9;
		public Thread thread;
		private boolean running = false;
		Random r = new Random();
		private Handler handler;
		private HUD hud;
		private Spawn spawner;
		
		
		public Game(){
			
		handler = new Handler();//1st making handler , later window, game must know what is a handler
		this.addKeyListener(new KeyInput(handler));
		
		new Window(WIDTH,HEIGHT,"Lets Play",this);//creating new window -> JFrame

			handler.addObject(new Player(WIDTH/2-32,HEIGHT/2-32,ID.Player2,handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH),	r.nextInt(HEIGHT),		ID.BasicEnemy,handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH),	r.nextInt(HEIGHT),		ID.BasicEnemy,handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH),	r.nextInt(HEIGHT),		ID.BasicEnemy,handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH),	r.nextInt(HEIGHT),		ID.BasicEnemy,handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH),	r.nextInt(HEIGHT),		ID.BasicEnemy,handler));
			System.out.println("BRANCH");
			hud = new HUD();

		spawner = new Spawn(handler,hud);


			
	}
	public synchronized void start(){
		thread = new Thread(this);//thread for this -> this==game class
		thread.start();//start the thread
		running = true;//thread online
	}
	
	public synchronized void stop(){
		try{
			thread.join();
			running = false;//thread offline//killing thread
		}catch(Exception e)
		{
			e.printStackTrace();		
		}
	}
	
	public void run(){
		//frames matching in 1 sec && main game loop
		this.requestFocus();
		
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while(running){//main game loop
		long now = System.nanoTime();
		delta += (now - lastTime) /ns;
		lastTime = now;
			while(delta >= 1){
				tick();//2nd
				delta--;		
			}
			if(running)
				render();//1st
				frames++;
				
				if(System.currentTimeMillis() - timer > 1000){
					timer += 1000;
				//System.out.println("FPS: "+frames);
					frames = 0;
				}
		  }
		stop();
	}
	
	private void tick(){
		handler.tick();
		hud.tick();
		spawner.tick();
	}
	
	private void render(){
		BufferStrategy bs = this.getBufferStrategy();
		if(bs==null)
		{
			this.createBufferStrategy(3);
			return;
		}
	
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.black);
		g.fillRect(0 , 0, WIDTH, HEIGHT);
		
		handler.render(g);
		hud.render(g);
		
		g.dispose();
		bs.show();
	}
	
	public static int clamp(int var, int min, int max){//blocking player //cannot go out from the map
		if(var >= max)
			return var = max;
		else if(var <= min)
			return var = min;
		
		return var;
	}
	
	
	public static void main(String args[]){
		new Game();
	}
}
	
	
