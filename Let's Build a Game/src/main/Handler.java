package main;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {//rendering and updating all objects in game
 
	LinkedList<GameObject> object = new LinkedList<GameObject>();
	
	public void tick(){
		for(int i=0; i < object.size(); i++){
			GameObject tempObject = object.get(i);//setting temporary object to object.get(i) from linkedlist
			
			tempObject.tick();
		}
	}
	
	public void render(Graphics g){
		for(int i=0; i < object.size(); i++){
			GameObject tempObject = object.get(i);//same as top
			
			tempObject.render(g);
		}
	}
	
	public void addObject(GameObject object){
		this.object.add(object);//adding to linked list
	}
	
	public void removeObject(GameObject object){
		this.object.remove(object);//remove object from linked list
	}
	
	
}
