package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

public class Player extends GameObject {
Random r = new Random();
Handler handler;

	public Player(int x, int y, ID id,Handler handler) {
		super(x, y, id);	
		this.handler = handler;//handler in player <- handler from game in creation
	}
	
	public Rectangle getBounds(){
		return new Rectangle(x,y,32,32);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		x = Game.clamp(x, 0, Game.WIDTH - 35);//cannot go out from the map
		y = Game.clamp(y, 0, Game.HEIGHT - 60);
		
		handler.addObject(new Trail(x,	y,	ID.Trail, Color.white,	32,	32,	0.09f,	handler));
		
		collision();
	}
	
	private void collision(){
		for(int i=0;	i<handler.object.size();	i++){
			
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getId() == ID.BasicEnemy){
				//collision code
				if(getBounds().intersects(tempObject.getBounds())){
					//collision code 
					HUD.HEALTH -=4;
				}
			}
		}
	}
	
	
	public void render(Graphics g) {
		
		g.setColor(Color.white);
		g.fillRect(x,y,32,32);
		//Graphics2D g2d = (Graphics2D) g;//Graphics2D got methods that Graphics doesn't have
		//g.setColor(Color.red);to see a rect's
		//g2d.draw(getBounds());
		
	}
}
